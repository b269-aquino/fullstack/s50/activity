/*import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate, useParams, useNavigate, Link} from 'react-router-dom';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState("");

    const register = (userReg) => {
          fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail/${email}`)
            .then((res) => res.json())
            .then((data) => {
              if (data.exists) {
                Swal.fire({
                  title: "Duplicate email found",
                  icon: "error",
                  text: "Please provide a different email",
                });
                } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  },
                  body: JSON.stringify({
                    userReg: userReg,
                  }),
                })
                  .then((res) => res.json())
                  .then((data) => {
                    console.log(data);

                    if (data === true) {
                      Swal.fire({
                        title: "Registration successfull",
                        icon: "success",
                        text: "Welcome to Zuitt !",
                      });

                      navigate("/login");
                    }
                  });
              }
            });
        };

    useEffect(() => {
      if (
        email !== "" &&
        firstName !== "" &&
        lastName !== "" &&
        mobileNo !== "" &&
        mobileNo.length >= 11 &&
        password1 !== "" &&
        password2 !== "" &&
        password1 === password2
      ) {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }, [email, firstName, lastName, mobileNo, password1, password2]);

    // function registerUser(e) {
    //     e.preventDefault();

    //     // Clear input fields
    //     setEmail("");
    //     setPassword1("");
    //     setPassword2("");

    //     alert("Thank you for registering!");
    // };

    return (
        (user.id !== null) ?
        <Navigate to="/login" />
        :
        <Form onSubmit={(userReg) => register(userReg)} >
           
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="firstName" 
                    placeholder="Enter first name" 
                    value={firstName}
                    onChange={userReg => setFirstName(userReg.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="lastName" 
                    placeholder="Enter last name" 
                    value={lastName}
                    onChange={userReg => setLastName(userReg.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={userReg => setEmail(userReg.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="mobileNo" 
                    placeholder="Enter Mobile Number" 
                    value={mobileNo}
                    onChange={userReg => setMobileNo(userReg.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
                    value={password1}
                    onChange={userReg => setPassword1(userReg.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
                    value={password2}
                    onChange={userReg => setPassword2(userReg.target.value)}
	                required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" onClick={() => register(userReg)}>Submit</Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }


        </Form>
    )

}*/


import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate, useParams, useNavigate, Link} from 'react-router-dom';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState("");

    const register = (userId) => {
      fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail/${email}`, {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          userId: userId
        })
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          if (data) {
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
              body: JSON.stringify({
                userId: userId,
              }),
            })
              .then((res) => res.json())
              .then((data) => {
                console.log(data);

                if (data === true) {
                  Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: "Welcome to Zuitt!",
                  });

                  navigate("/login");
                }
              });
          }
        });
    };


    useEffect(() => {
      if (
        email !== "" &&
        firstName !== "" &&
        lastName !== "" &&
        mobileNo !== "" &&
        mobileNo.length >= 11 &&
        password1 !== "" &&
        password2 !== "" &&
        password1 === password2
      ) {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }, [email, firstName, lastName, mobileNo, password1, password2]);

    return (
        (user.id !== null) ?
        <Navigate to="/login" />
        :
        <Form onSubmit={(userReg) => register(userReg)} >
           
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="firstName" 
                    placeholder="Enter first name" 
                    value={firstName}
                    onChange={userReg => setFirstName(userReg.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="lastName" 
                    placeholder="Enter last name" 
                    value={lastName}
                    onChange={userReg => setLastName(userReg.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={userReg => setEmail(userReg.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="mobileNo" 
                    placeholder="Enter Mobile Number" 
                    value={mobileNo}
                    onChange={userReg => setMobileNo(userReg.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={userReg => setPassword1(userReg.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={userReg => setPassword2(userReg.target.value)}
                    required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" onSubmit={() => register()}>Submit</Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }


        </Form>
    )

}

