import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Karl Aquino";
// <h1>HELLO, {name}</h1> - JSX (JS XML)
// const element = <h1>HELLO, {name}</h1>


// const user = {
//   firstName: "Jane",
//   lastName: "smith"
// }

// function formatName(user) {
//   return user.firstName + ' ' + user.lastName;
// };

// const element = <h1>HELLO, {formatName(user)}</h1>

// root.render(element);


